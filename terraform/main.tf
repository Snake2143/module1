terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABZHAn2AATuwQAAAAD0j2CC4Ra7u29NTXi8nYJFiOHsWSKJZ9g"
  cloud_id  = "b1g4gnb9ic202k1a3o0n"
  folder_id = "b1grqv94v68g4evp12iq"
  zone      = "ru-central1-b"
}
resource "yandex_logging_group" "group1" {
  name      = "logs"
}


data "yandex_compute_image" "my-ubuntu-2004-1" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "my-vm-1" {
  name        = "test-vm-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-b"

   resources {
    cores  = 4
    memory = 4
  }
boot_disk {
    initialize_params {
      size=20
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

 network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = true
  }

 metadata = {
    ssh-keys = "ubuntu:${file("/home/ubuntu/test.pub")}"
  }
}


resource "yandex_compute_instance" "prod" {
  name        = "prod"
  platform_id = "standard-v1"
  zone        = "ru-central1-b"

   resources {
    cores  = 4
    memory = 4
  }
boot_disk {
    initialize_params {
      size=20
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

 network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = true
  }

 metadata = {
    ssh-keys = "ubuntu:${file("/home/ubuntu/test.pub")}"
  }
}




resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-b"
  v4_cidr_blocks = ["192.168.10.0/24"]
  network_id     = "${yandex_vpc_network.network-1.id}"
}


resource "yandex_lb_target_group" "my-lb-tg-1" {
  name      = "my-target-group-1"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    address   = "${yandex_compute_instance.my-vm-1.network_interface.0.ip_address}"
  }


target {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    address   = "${yandex_compute_instance.prod.network_interface.0.ip_address}"
  }
}

resource "yandex_lb_network_load_balancer" "my-nw-lb-1" {
  name = "my-network-load-balancer-1"

  listener {
    name = "my-listener-1"
    port = 80
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.my-lb-tg-1.id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
      }
    }
  }
}


output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address
}

output "internal_ip_address_prod" {
  value = yandex_compute_instance.prod.network_interface.0.ip_address
}

output "external_ip_address_prod" {
  value = yandex_compute_instance.prod.network_interface.0.nat_ip_address
}

output "my_balancer_ip_address" {
  value = yandex_lb_network_load_balancer.my-nw-lb-1.listener
}
